# Simple CRDT in Rust

> Matteo Bertini | naufraghi@develer.com
> @naufraghi on most socials

RustFest 2018 Rome

+++

<!-- .slide: data-background="https://images.unsplash.com/photo-1538037119734-bdbeddb81edd" data-background-color="#005" -->
<small style="margin-top: 60%">
<small style="color: darkgrey">Photo by Dave Ruck on Unsplash</small>
</small>

---

# Shopping List <span><!-- .element: class="fragment" data-fragment-index="1" -->... Set</span>

Note:
`git` example and conflicts on a list

+++

### Operations on CRDTs

- **Commutativity**: $a \oplus b=b \oplus a$, *so that order of application doesn't matter*.<!-- .element: class="fragment current-visible" data-fragment-index="1" -->
- **Associativity**: $a \oplus (b \oplus c)=(a \oplus b) \oplus c$, *so that grouping doesn't matter*.<!-- .element: class="fragment current-visible" data-fragment-index="2" -->
- **Idempotence**: $a \oplus a=a$, *so that duplication doesn't matter*.<!-- .element: class="fragment current-visible" data-fragment-index="3" -->

---

# Append Only Set

+++

## Append Only Set

```rust
struct AddOnlySet {
    values: HashSet<Item>,  // type Item = String;
}
impl AddOnlySet {
    fn add(&mut self, value: Item) {
        self.values.insert(value);
    }
    ...
    fn merge(&self, other: &Self) -> Self {
        let values = self.values.union(&other.values)
                                .cloned().collect();
        Self { values }
    }
}
```

+++

# Composition

<!-- .slide: data-background="https://images.unsplash.com/photo-1493217465235-252dd9c0d632" data-background-color="#005" -->
<small style="margin-top: 60%">
<small style="color: darkgrey">Photo by Iker Urteaga on Unsplash</small>
</small>

Note:
Let use the power of composition

---

## Remove Once Set

```rust
struct AddRemoveOnceSet {
    added: AddOnlySet,
    removed: AddOnlySet,
}
```

+++

### Remove Once Set<span><!-- .element: class="fragment" data-fragment-index="1" --> + Merge Trait</span>

```rust
impl Merge for AddRemoveOnceSet {
    fn merge(&self, other: &Self) -> Self {
        let added = self.added.merge(&other.added);
        let removed = self.removed.merge(&other.removed);
        Self { added, removed }
    }
}
```

---

### Remove Once Set

```rust
impl AddRemoveOnceSet {
    ... // query has no side effects
    fn items(&self) -> HashSet<&Item> {
        self.added.values.difference(&self.removed.values)
                         .collect()
    }
}
```

---

# Stamp trick

<!-- .slide: data-background="https://images.unsplash.com/photo-1522244196964-68003e683e15" data-background-color="#005" -->
<small style="margin-top: 60%">
<small style="color: darkgrey">Photo by Chiheb Chakchouk on Unsplash</small>
</small>

---

## More complex Item

```rust
type Stamp = usize;
type Value = String;
type Item = (Stamp, Value);
```

Note:
_Stamp_ may be a Lamport timestamp

+++

## Decorate the value

```rust
impl ... {
    fn add(&mut self, value: Value) {
        let stamp = self.current_stamp() + 1;
        self.added.add((stamp, value));
    }
    ...
    fn current_stamp(&self) -> Stamp {
        self.added.current_stamp()
            .max(self.removed.current_stamp())
    }
}
```

+++

## Modified Append Only

```rust
impl AddOnlySet {
    fn add(&mut self, value: Item) {
        self.values.insert(value);
    }
    fn current_stamp(&self) -> Stamp {
        let stamps = self.values.iter().map(|i| &i.0);
        stamps.max().cloned().unwrap_or(Stamp::default())
    }
}
```

---

## Observed Remove Set

```rust
impl Items for ObservedRemoveSet {
    fn items(&self) -> HashSet<&Value> {
        self.added
            .values
            .difference(&self.removed.values)
            .map(|i| &i.1)
            .collect()
    }
}
```

---

## Observed Remove Set

```rust
impl ObservedRemoveSet {
    ...
    fn remove(&mut self, value: Value) {
        for (s, v) in self.added.values.iter().cloned() {
            if v == value {
                self.removed.add((s, v));
            }
        }
    }
    ...
}
```

---

# Traits

+++

```rust
trait Merge {
    fn merge(&self, other: &Self) -> Self;
}
```
@ul

- Union for sets
- Max for numbers

@ulend

+++

```rust
trait Items {
    type Value: Hash;
    fn items(&self) -> HashSet<&Self::Value>;
}
```

+++

```rust
trait Stamper {
    type Stamp: Default + Increment + Merge + Ord + Clone;
    fn current_stamp(&self) -> Self::Stamp;
}
```

```rust
trait Increment {
    fn increment(&self) -> Self;
}
```

---

# Let's Make it Generic

Note:
Link a Rust Playground?

---

## References

- [Conflict-free Replicated Data Types](https://pages.lip6.fr/Marc.Shapiro/papers/CRDTs-Springer2018-authorversion.pdf) paper.
- [Microsoft Research Video 153540: Strong Eventual Consistency and Conflict-free Replicated Data Types](https://archive.org/details/Microsoft_Research_Video_153540) by Marc Shapiro.
- [A comprehensive study of Convergent and Commutative Replicated Data Types](https://hal.inria.fr/file/index/docid/555588/filename/techreport.pdf) paper.

+++

## Implementations

- [soundcloud/roshi](https://github.com/soundcloud/roshi) implements a time-series event storage via a LWW-element-set CRDT (in Go, on top of Redis).
- [rust-crdt/rust-crdt](https://github.com/rust-crdt/rust-crdt) Thoroughly tested (with `quickcheck`) serializable practical CRDT's ported from riak_dt.

+++

## Possible Optimizations

- Given the _remove-after-add_ hypothesis, we can in fact remove the item from the set and have only one set
